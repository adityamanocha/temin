<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- Stylesheet to add standard nodes to a receive command -->
<!-- $ PFD_Date: $ -->
<!-- $PFD_File: General\SCLIB\scReceiveInit.xslt $ -->
<!-- $PFD_Installation: Best Practice 4.5 $ -->
<!-- $Header: /Communication Server/ComSrv/SWIFT/SCLIB/scReceiveInit.xslt 3     2-07-04 17:03 Jnh $ -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" indent="yes" />
  <!-- Main template -->
  <xsl:template match="/">
    <Command>
      <xsl:copy-of select="/Command/@* | /Command/*[name(.) != 'AckNak']" />
      <AckNak>
        <pGENMATCHMES />
        <pSWIFTNAKCODE>
          <ErrorInAction />
          <Text />
        </pSWIFTNAKCODE>
      </AckNak>
    </Command>
  </xsl:template>
</xsl:stylesheet>
